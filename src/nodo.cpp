#include "nodo.hpp"
#include "HashMap.hpp"
#include "mpi.h"
#include <unistd.h>
#include <stdlib.h>
#include "defines.hpp"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

void nodo(unsigned int rank) {

    printf("Soy un nodo. Mi rank es %d \n", rank);
    // Creo un HashMap local
    HashMap map;

    while (true) {
        int number = -1; //Aca vamos a recibir la función a ejecutar
        MPI_Recv(
                &number, // Donde lo guardo 
                1, //El tamaño del mensaje
                MPI_INT, //La función a ejecutar
                NODO_CONSOLA, //De donde recibo (0 es el nodo consola)
                MPI_ANY_TAG, //
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE);

        switch(number) {

            case ID_LOAD:
                load(map);
            break;

            case ID_MEMBER:
                member(map);
            break;

            case ID_ADDANDINC:
                addandinc(map, rank);
            break;

            case ID_MAXIMUM:
                maximum(map);
            break;
            
            case ID_QUIT:
                return;
            break;

            case ID_LIMPIAR:
                map = HashMap();
            break;
            
            case ID_ERROR:
				printf("Error en recepción de ID por parte de un nodo.");
			break;
			
			default:
				printf("No debería entrar acá, error en recepción de ID por parte de un nodo");
        }

    }
}

void load(HashMap& map) {

    int size;

    MPI_Recv(&size,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    char* nombre = (char*)malloc(size);

    MPI_Recv(nombre,size,MPI_CHAR,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    string nombre_posta(nombre);

    free((void*)nombre);

    map.load(nombre_posta);

    int msg = 0;
    
    trabajarArduamente();
    MPI_Send(&msg,1,MPI_INT,0,TAG_FILE_LOADED,MPI_COMM_WORLD);
}

void addandinc(HashMap& map, int rank) {

    int error = 0; 
    trabajarArduamente();
    MPI_Send(&error,1,MPI_INT,0,TAG_ADDANDINC,MPI_COMM_WORLD);

    rank_t elegido = -1;

    MPI_Recv(&elegido,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    if(rank == -1) {
        std::cout << "Error en nodo: " << rank << "addandinc, al recibir elegido." << std::endl;
    }
    else if( rank == elegido) {
    
        int size;

        //Tiene que ser bloqueante
        MPI_Recv(&size,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        char* nombre = (char*)malloc(size);

        //Same
        MPI_Recv(nombre,size,MPI_CHAR,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        string nombre_posta(nombre);

        free((void*)nombre);

        map.addAndInc(nombre_posta);
    }
}

//recibir clave 

void member(HashMap& map) {

    int size;

    MPI_Recv(&size,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    char* nombre = (char*)malloc(size);

    MPI_Recv(nombre,size,MPI_CHAR,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    string nombre_posta(nombre);

    free((void*)nombre);

    int miembro = (int)map.member(nombre_posta);
    trabajarArduamente();
    MPI_Send(&miembro,1,MPI_INT,0,TAG_MEMBER,MPI_COMM_WORLD);
}


void maximum(HashMap& map) {

    HashMap::iterator iter = map.begin();

    while(iter != map.end()) {

        uint tam = (*iter).size() + 1;
        
        char* nombre = (char*)malloc(tam);

        strcpy(nombre,(*iter).c_str());

        trabajarArduamente();
        MPI_Send(nombre,tam,MPI_CHAR,0,0,MPI_COMM_WORLD); //Este debe ser bloqueante

        free(nombre);

        iter++;
    }

    trabajarArduamente();
    MPI_Send(NULL,0,MPI_CHAR,0,TAG_ULTIMA_PALABRA,MPI_COMM_WORLD); //Probar (?)
}

void trabajarArduamente() {
    //Esto es porque si no te moris antes de que termine el test grande.
    #ifndef TESTS
    int r = rand() % 2500000 + 500000;
    #endif
    #ifdef TESTS
    int r = 100;
    #endif

    usleep(r);
}
