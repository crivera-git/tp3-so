#!/bin/sh
#Recordar usar chmod +x sampler.sh para darle permisos de ejecución -r

if [ $# -ne "3" ]; then
	echo "Modo de uso: $0 nombre_archivo max_lineas cant_archivos" 
	exit
fi
#echo Generando una muestra de $2 líneas a partir de $1"\n"
cant=$3

DIR=$(dirname "$1")
NAME=$(basename "$1")

mkdir -p $DIR/${NAME}SPLIT/ > /dev/null
while [ "$cant" -ne 0 ]; do 
	shuf -n $2 $1 > $DIR/${NAME}SPLIT/$cant".in"
	cant=$(($cant - 1))
done

FILES=`ls $DIR/${NAME}SPLIT | awk "/^[0-9]+.in$/ {print \"$DIR/${NAME}SPLIT/\" "'$1}'`
#cat $FILES | sort | uniq -c | awk '{print $1}' | sort | head -1

#echo "Expected"
cat $FILES | sort | uniq -c | awk '{print $1,$2}' | sort -n | tail -1 
