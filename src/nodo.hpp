#ifndef _NODO_H
#define _NODO_H
#include "HashMap.hpp"
#define BUFFER_SIZE 1024

/* Función que maneja un nodo.
 * Recibe el rank del nodo.
 */
void nodo(unsigned int rank);

void load(HashMap& map);
void member(HashMap& map);
void addandinc(HashMap& map, int rank);
void maximum(HashMap& map);
void quit();


/* Simula un tiempo de procesamiento no determinístico.
 */
void trabajarArduamente();

#endif  /* _NODO_H */
