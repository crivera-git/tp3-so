#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utility>
#include <string>
#include <list>
#include <iostream>
#include <sstream>
#include "consola.hpp"
#include "HashMap.hpp"
#include "mpi.h"
#include <algorithm>
#include "defines.hpp"
#include <unistd.h>
#include <cassert>



using namespace std;

#define CMD_LOAD    "load"
#define CMD_ADD     "addAndInc"
#define CMD_MEMBER  "member"
#define CMD_MAXIMUM "maximum"
#define CMD_QUIT    "quit"
#define CMD_SQUIT   "q"
#define CMD_TEST    "tests" //Nuestro


static unsigned int np;
typedef int rank_t;
// Crea un ConcurrentHashMap distribuido
static void load(list<string> params) {

    //Guardo la cantidad inicial porque voy a modificar la lista
    uint cant_archivos = params.size();

    //Para decirle a los nodos a qué función llamar
    int number = ID_LOAD;
    MPI_Status status;
    MPI_Request req;

    //minimo entre nodos y archivos a procesar
    int minimum = np-1 < params.size() ? np-1 : params.size();
    
    //Todos los que podemos mandar sin espera
    for(int destino = 1; destino <= minimum; ++destino) {
       
        //no modificamos la variable,por lo que podemos hacerlo no bloqueante
		MPI_Isend(&number,1,MPI_INT,destino,TAG_LOAD,MPI_COMM_WORLD,&req);

        int tam = params.front().size()+1;

        //Por las dudas lo dejamos bloqueante por la próxima iteración
        MPI_Send(&tam,1,MPI_INT,destino,TAG_LOAD,MPI_COMM_WORLD);

        char* nombre = (char*)malloc(tam);

        strcpy(nombre,params.front().c_str());

        //Bloqueante si o si porque vamos a liberar la memoria del buffer
        MPI_Send(nombre,tam,MPI_CHAR,destino,TAG_LOAD,MPI_COMM_WORLD);

        free(nombre);

        params.pop_front();
    }

    int error = -1;
	uint archCargados = 0;
	
    while( !params.empty() ) {

    	//Bloqueante, esperamos a que se libere alguno
        MPI_Recv(&error, 1, MPI_INT, MPI_ANY_SOURCE, TAG_FILE_LOADED, MPI_COMM_WORLD, &status);
        archCargados++;
        
        rank_t destino = status.MPI_SOURCE;

        //no modificamos la variable,por lo que podemos hacerlo no bloqueante
        MPI_Isend(&number,1,MPI_INT,destino,TAG_LOAD,MPI_COMM_WORLD,&req);

        int tam = params.front().size()+1;

        //Por las dudas lo dejamos bloqueante por la próxima iteración
        MPI_Send(&tam,1,MPI_INT,destino,TAG_LOAD,MPI_COMM_WORLD);

        char* nombre = (char*)malloc(tam);

        strcpy(nombre,params.front().c_str());

        //Bloqueante si o si porque vamos a liberar la memoria del buffer
        MPI_Send(nombre,tam,MPI_CHAR,destino,TAG_LOAD,MPI_COMM_WORLD);

        free(nombre);

        params.pop_front();

    }
	
    for( uint i = 0 ; i < (cant_archivos-archCargados); i++){ 
    	//Es lo mismo que sea bloqueante por el any source
        MPI_Recv(&error, 1, MPI_INT, MPI_ANY_SOURCE, TAG_FILE_LOADED, MPI_COMM_WORLD, &status);
    }

    cout << "La listá esta procesada" << endl;
}

// Esta función suma uno a *key* en algún nodo
static void addAndInc(string key) {
    
    int number = ID_ADDANDINC;
    int tam = key.size()+1; //+1 por el \0

    char* clave = (char*)malloc(tam);
    strcpy(clave, key.c_str());

    MPI_Request req;
    MPI_Request req_elegido;

	//Le mandamos el pedido a todos
    for(uint destino = 1; destino < np; ++destino) {    
    
		//Este se puede hacer no bloqueante    
        MPI_Isend(&number,1,MPI_INT,destino,0,MPI_COMM_WORLD,&req); 

    }

    MPI_Status status;
    int error = -1;

    //El primero que responde es el que lo va a hacer
    MPI_Recv(&error, 1, MPI_INT, MPI_ANY_SOURCE, TAG_ADDANDINC, MPI_COMM_WORLD, &status);
    //Puede ser bloqueante porque de todas formas no hay nada más que hacer mientras...
   
    rank_t elegido = status.MPI_SOURCE;
	
	//Le mandamos a todos el elegido
    for(uint destino = 1; destino < np; ++destino){
    	if(destino == (uint)elegido){
        	MPI_Isend (&elegido,1,MPI_INT,destino,0,MPI_COMM_WORLD,&req_elegido);
    	}
    	else MPI_Isend (&elegido,1,MPI_INT,destino,0,MPI_COMM_WORLD,&req);
    }
	
	//Para asegurarnos de que tam no sale de scope al terminar la función
    MPI_Send(&tam,1,MPI_INT,elegido,TAG_ADDANDINC,MPI_COMM_WORLD); 

    MPI_Send(clave,tam,MPI_CHAR,elegido,0,MPI_COMM_WORLD); //Este debe ser bloqueante

    free(clave);
	
	//suponiendo que no hay errores en la comunicación
    cout << "Agregado: " << key << endl; 
    
    for(uint i = 0 ; i < (np-2) ; i++) {	
		//Bloqueante porque es any source, es lo mismo
		MPI_Recv(&error, 1, MPI_INT, MPI_ANY_SOURCE, TAG_ADDANDINC, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}   
	//asegura que al elegido le llego el mensajito de texto
	MPI_Wait( &req_elegido, MPI_STATUS_IGNORE);
}

// Esta función busca la existencia de *key* en algún nodo
static void member(string key) {

    int number = ID_MEMBER;
    int tam = key.size()+1;

    char* clave = (char*)malloc(tam);
    strcpy(clave, key.c_str());
    MPI_Request req;

    for(uint destino = 1; destino < np; ++destino) {    
    	
    	//no bloqueantes porque los recv siguientes hacen de "barrera"

        MPI_Isend(&number,1,MPI_INT,destino,0,MPI_COMM_WORLD, &req); 

        MPI_Isend(&tam,1,MPI_INT,destino,0,MPI_COMM_WORLD, &req); 

        MPI_Isend(clave,tam,MPI_CHAR,destino,0,MPI_COMM_WORLD, &req);
    
    }
    
    bool imprimi = false;
     
    for( uint i = 1 ; i < np ; ++i){
        int temp;
        MPI_Recv(&temp, 1, MPI_INT, MPI_ANY_SOURCE, TAG_MEMBER, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        if(temp && !imprimi) {
			 cout << "El string <" << key << "> está" << endl;
			 imprimi = true;	
		}
    }   
	
	if (!imprimi) {
		cout << "El string <" << key << "> no está" << endl;
    }
    free(clave);
    
}


// Esta función calcula el máximo con todos los nodos
static void maximum() {
    
    int number = ID_MAXIMUM;
    MPI_Request req;

    for(uint destino = 1; destino < np; ++destino) {    
    	//Por el tag de última palabra, nos garantiza correctitud
        MPI_Isend(&number,1,MPI_INT,destino,0,MPI_COMM_WORLD, &req); 
    }

    HashMap map;

    MPI_Status status;

    uint faltantes = np - 1;

    while(faltantes) {

        MPI_Probe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);

        int tam;
        MPI_Get_count(&status,MPI_CHAR,&tam);
        
        if(status.MPI_TAG == TAG_ULTIMA_PALABRA) {
            --faltantes;
            //Es lo mismo por el probe anterior y porque es de 0 bytes.
            MPI_Recv(NULL, 0, MPI_CHAR, status.MPI_SOURCE, TAG_ULTIMA_PALABRA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            continue;
        }

        char* msg = (char*)malloc(sizeof(char) * tam);

        MPI_Recv(msg, tam, MPI_CHAR, status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        string key(msg);

        map.addAndInc(key);
    }

    std::pair<std::string, unsigned int> maximo = map.maximum();
    cout << "El máximo es <" << maximo.first <<"," << maximo.second << ">" << endl;
}


// Esta función debe avisar a todos los nodos que deben terminar
static void quit() {

    int number = ID_QUIT;

    MPI_Request* requests = (MPI_Request*) malloc(sizeof(MPI_Request) * (np - 1) );

    for(uint destino = 1; destino < np; ++destino) {    
        MPI_Isend(&number,1,MPI_INT,destino,0,MPI_COMM_WORLD, requests + destino - 1); //Este se puede hacer no bloqueante
    }

    MPI_Waitall(np-1,requests,MPI_STATUS_IGNORE); //Por si number sale de scope
}

static void limpiar() {

    int number = ID_LIMPIAR;

    for(uint destino = 1; destino < np; ++destino) {    
        MPI_Send(&number,1,MPI_INT,destino,0,MPI_COMM_WORLD); //Este se puede hacer no bloqueante
    }
}

static bool member_test(string key) {
    int pipes[2];
    pipe(pipes);

    int old = dup(STDOUT_FILENO);
    dup2(pipes[1],STDOUT_FILENO);

    member(key);


    char buffer[256];

    int tam = read(pipes[0], buffer, 256);

    buffer[tam-1] = '\0';

    string resultado(buffer);

    dup2(old,STDOUT_FILENO);
    close(pipes[0]);

    if(resultado == string("El string <" +key+ "> está")) return true;
    return false;
}



static std::pair<std::string, unsigned int> maximum_test() {
    int pipes[2];
    pipe(pipes);

    int old = dup(STDOUT_FILENO);
    dup2(pipes[1],STDOUT_FILENO);

    maximum();

    char buffer[256];

    int tam = read(pipes[0], buffer, 256);

    buffer[tam-1] = '\0';

    string clave;
    string cantidad;

    int pos = sizeof("El máximo es <") - 1;
    while(buffer[pos] != ','){ 
        
        clave.push_back(buffer[pos]);
        pos++;
    }
    pos++;

    while(buffer[pos] != '>'){ 
        
        cantidad.push_back(buffer[pos]);
        pos++;
    }        

    std::pair<std::string, unsigned int> resultado = make_pair(clave,std::atoi(cantidad.c_str()));

    dup2(old,STDOUT_FILENO);
    close(pipes[0]);

    
    return resultado; 
}

static void test() {

    std::cout << "----------------TEST 1----------------" << std::endl;
    std::cout << "Testeando todo menos load" << std::endl << std::endl; 
    addAndInc("hola");
    assert(member_test("hola"));
    
    //Agrego un adios, test member en caso de no estar
    assert(!member_test("adios"));
    addAndInc("adios");
    assert(member_test("adios"));
    
    //Agrego 2 camellos
    addAndInc("camello");
    addAndInc("camello");
    assert(member_test("camello"));
    assert(member_test("hola"));

    assert(maximum_test().first == "camello");
    assert(maximum_test().second == 2);

    //Hago maximo a hola, 3
    addAndInc("hola");
    addAndInc("hola");
    assert(member_test("hola"));
    
    assert( (maximum_test().first == "hola") && maximum_test().second == 3);

    //2 maximos camello u hola
    addAndInc("camello");
    assert( (maximum_test().first == "hola" || maximum_test().first == "camello") && maximum_test().second == 3);
    
    //Ahora es hola si o si y agrego una parecida a camellos
    addAndInc("camellos");
    addAndInc("hola");
    assert( (maximum_test().first == "hola") && maximum_test().second == 4);
    member_test("camellos");

    std::cout << "Pasaron todos los asserts del TEST 1 :)" << std::endl << std::endl;

    std::cout << "----------------TEST 2----------------" << std::endl;
    std::cout << "Testeando load de un archivo" << std::endl << std::endl;

    std::list<std::string> archivos;
    archivos.push_back("testcases/EJ2.txt");
    
    limpiar();
    load(archivos);

    assert(member_test("pedro"));
    assert(member_test("paramo"));
    assert(member_test("nacio"));
    assert(member_test("en"));
    assert(member_test("una"));
    assert(member_test("vilya"));
    
    assert(maximum_test() == std::make_pair( string("vilya"),(uint)3) );

    std::cout << "Pasaron todos los asserts del TEST 2 :)" << std::endl << std::endl;

    std::cout << "----------------TEST 3----------------" << std::endl;
    std::cout << "Testeando load de muchos archivos" << std::endl << std::endl;

    archivos = std::list<std::string>();  
    archivos.push_back("testcases/EJ3_1.txt"); 
    archivos.push_back("testcases/EJ3_2.txt");
    archivos.push_back("testcases/EJ3_1.txt");
    archivos.push_back("testcases/EJ3_1.txt");
    archivos.push_back("testcases/EJ6.txt"  );

    limpiar();
    load(archivos);

    //los archivos 3 y 4 son el archivo 1 y el último tiene sólo 5 "vilya" y 5 "pedro"
    //(en total hay 2*3+5 = 11 "pedro") por lo que el maximo es vilya, 3+2+3+3+5 = 16   
    assert( maximum_test() == std::make_pair( string("vilya"),(uint)16) );

    std::cout << "Pasaron todos los asserts del TEST 3 :)" << std::endl << std::endl;

    std::cout << "----------------TEST 4----------------" << std::endl;
    std::cout << "Testeando load de muchos archivos (versión grande)" << std::endl << std::endl;

    archivos = std::list<std::string>();

    for(uint i = 1; i <= 50 ; ++i) {

        std::ostringstream ostr; //output string stream
        ostr << i; //use the string stream just like cout,

        std::string numero = ostr.str(); //t

        archivos.push_back(string("testcases/testgrande/")+numero+string(".in"));
    }

    limpiar();
    load(archivos);

    assert( maximum_test() == std::make_pair( string("que"),(uint)14216) );

    //<que,14216> es el maximum obtenido mediante el siguiente comando: 
    //cat $FILES | sort | uniq -c | awk '{print $1,$2}' | sort -n | tail -1 
    //Donde $FILES es la lista de archivos

    //Tendría sentido que esten dentro de tantas palabras.... :)
    assert(member_test("quijote"));
    assert(member_test("sancho"));

    std::cout << "Pasaron todos los asserts del TEST 4 :)" << std::endl << std::endl;

}



/* static int procesar_comandos()
La función toma comandos por consola e invoca a las funciones correspondientes
Si devuelve true, significa que el proceso consola debe terminar
Si devuelve false, significa que debe seguir recibiendo un nuevo comando
*/

static bool procesar_comandos() {

    char buffer[BUFFER_SIZE];
    size_t buffer_length;
    char *res, *first_param, *second_param;

    // Mi mamá no me deja usar gets :(
    res = fgets(buffer, sizeof(buffer), stdin);

    // Permitimos salir con EOF
    if (res==NULL)
        return true;

    buffer_length = strlen(buffer);
    // Si es un ENTER, continuamos
    if (buffer_length<=1)
        return false;

    // Sacamos último carácter
    buffer[buffer_length-1] = '\0';

    // Obtenemos el primer parámetro
    first_param = strtok(buffer, " ");

    if (strncmp(first_param, CMD_QUIT, sizeof(CMD_QUIT))==0 ||
        strncmp(first_param, CMD_SQUIT, sizeof(CMD_SQUIT))==0) {

        quit();
        return true;
    }

    if (strncmp(first_param, CMD_MAXIMUM, sizeof(CMD_MAXIMUM))==0) {
        maximum();
        return false;
    }

    if (strncmp(first_param, CMD_TEST, sizeof(CMD_TEST))==0) {
        test();
        quit();
        return true;
    }

    // Obtenemos el segundo parámetro
    second_param = strtok(NULL, " ");
    if (strncmp(first_param, CMD_MEMBER, sizeof(CMD_MEMBER))==0) {
        if (second_param != NULL) {
            string s(second_param);
            member(s);
        }
        else {
            printf("Falta un parámetro\n");
        }
        return false;
    }

    if (strncmp(first_param, CMD_ADD, sizeof(CMD_ADD))==0) {
        if (second_param != NULL) {
            string s(second_param);
            addAndInc(s);
        }
        else {
            printf("Falta un parámetro\n");
        }
        return false;
    }

    if (strncmp(first_param, CMD_LOAD, sizeof(CMD_LOAD))==0) {
        list<string> params;
        while (second_param != NULL)
        {
            string s(second_param);
            params.push_back(s);
            second_param = strtok(NULL, " ");
        }

        load(params);
        return false;
    }

    printf("Comando no reconocido");
    return false;
}

void consola(unsigned int np_param) {
    np = np_param;
    printf("Comandos disponibles:\n");
    printf("  "CMD_LOAD" <arch_1> <arch_2> ... <arch_n>\n");
    printf("  "CMD_ADD" <string>\n");
    printf("  "CMD_MEMBER" <string>\n");
    printf("  "CMD_MAXIMUM"\n");
    printf("  "CMD_SQUIT"|"CMD_QUIT"\n");
    printf("  "CMD_TEST"\n");


    bool fin = false;
    while (!fin) {
        printf("> ");
        fflush(stdout);
        fin = procesar_comandos();
    }
}
