
#define TAG_LOAD 		0
#define TAG_ADDANDINC 	1
#define TAG_MEMBER 		2
#define TAG_MAXIMUM 	3

#define TAG_FILE_LOADED 10
#define TAG_ULTIMA_PALABRA 11

#define NODO_CONSOLA 0

#define ID_ERROR   (-1)
#define ID_LOAD      1
#define ID_MEMBER    2
#define ID_ADDANDINC 3
#define ID_MAXIMUM   4
#define ID_QUIT      5

#define ID_LIMPIAR 6

typedef int rank_t;
